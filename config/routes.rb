Rails.application.routes.draw do

  root    'sessions#new'  
  
  get     '/home',              to: 'static_pages#home'       #home_path
  get     '/login',             to: 'sessions#new'            #login_path
  post    '/login',             to: 'sessions#create'
  delete  '/logout',            to: 'sessions#destroy'
  get     '/edit_connections',  to: 'users#edit_connections'
  
  resources :users do
    member do
      get :search
      get :edit_connections
      patch :update_connections
      put :update_connections
      get :courses
      get :groups
      get :skills
      get :clients
      end
  end
  
  resources :courses do
    member do
      get :users
    end
  end
  
  resources :groups do
    member do
      get :users
    end
  end
  
  resources :skills do
    member do
      get :users
    end
  end
  
  resources :clients do
    member do
      get :users
    end
  end
  
  resources :microposts,          only: [:create, :destroy]
  resources :user_courses,        only: [:create, :destroy]
  resources :user_groups,         only: [:create, :destroy]
  resources :user_skills,         only: [:create, :destroy]
  resources :user_clients,        only: [:create, :destroy]
end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
