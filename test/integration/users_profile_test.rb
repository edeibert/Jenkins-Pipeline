require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:fUser0)
  end

  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_match @user.microposts.count.to_s, response.body
    assert_select 'div.pagination', 2
    @user.microposts.paginate(page: 1, per_page: 10).each do |micropost|
      assert_match micropost.content, response.body
    end
  end
end
