require 'test_helper'

class SkillTest < ActiveSupport::TestCase

  def setup
    @skill = Skill.new( name: "oSkill0", description: "oSkill0Description")
  end
  
  test "should be valid" do
    assert @skill.valid?
  end

  test "name should be present" do
    @skill.name = ""
    assert_not @skill.valid?
  end

  test "description should be present" do
    @skill.description = ""
    assert_not @skill.valid?
  end
  
  test "name should not be too long" do
    @skill.name = "a" * 101
    assert_not @skill.valid?
  end

  test "description should not be too long" do
    @skill.description = "a" * 10001
    assert_not @skill.valid?
  end
  
  test "name should be unique" do
    @skill.save
    duplicate_skill = @skill.dup
    assert_not duplicate_skill.valid?
  end

  test "associated usercourses should be destroyed" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)
    @skill.save
    @skill.user_skills.create!(user_id: user0.id)
    assert_difference 'UserSkill.count', -1 do
      @skill.destroy
    end
  end
  
  test "should connect and disconnect a user" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)  
    skill0 = skills(:fSkill0)                                                  #skill from skills.yml (persisted)
    assert_not user0.connectedToSkill?(skill0)
    assert_not skill0.connectedToUser?(user0)
    skill0.connectToUser(user0)
    assert user0.connectedToSkill?(skill0)
    assert skill0.connectedToUser?(user0)
    skill0.disconnectFromUser(user0)
    assert_not user0.connectedToSkill?(skill0)
    assert_not skill0.connectedToUser?(user0)
  end
end