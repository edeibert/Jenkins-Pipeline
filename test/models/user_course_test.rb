require 'test_helper'

class UserCourseTest < ActiveSupport::TestCase

  def setup
    @usercourse = UserCourse.new( user_id: users(:fUser0).id, 
                                  course_id: courses(:fCourse0).id)
  end
  
  test "should be valid" do
    assert @usercourse.valid?
  end

  test "user_id should be present" do
    @usercourse.user_id = nil
    assert_not @usercourse.valid?
  end

  test "course_id should be present" do
    @usercourse.course_id = nil
    assert_not @usercourse.valid?
  end
end

