require 'test_helper'

class UserGroupTest < ActiveSupport::TestCase
  
  def setup
    @usergroup = UserGroup.new( user_id: users(:fUser0).id, 
                                group_id: groups(:fGroup0).id, 
                                from_date: DateTime.new(2016,1,1), 
                                to_date: DateTime.new(2016,12,31))
  end
  
  test "should be valid" do
    assert @usergroup.valid?
  end

  test "user_id should be present" do
    @usergroup.user_id = nil
    assert_not @usergroup.valid?
  end

  test "group_id should be present" do
    @usergroup.group_id = nil
    assert_not @usergroup.valid?
  end
  
  test "from_date should be present" do
    @usergroup.from_date = nil
    assert_not @usergroup.valid?
  end

  test "to_date should be present" do
    @usergroup.to_date = nil
    assert_not @usergroup.valid?
  end
  
  test "to_date should be after from_date" do
    @usergroup.to_date = @usergroup.from_date - 1
    assert_not @usergroup.valid?
  end
end
