require 'test_helper'

class GroupTest < ActiveSupport::TestCase

  def setup
    @group = Group.new( name: "oGroup0", email: "oEmail0@test.nl", contact: "oContact0",  description: "oGroup0Description")
  end
  
  #Variables
  
  test "should be valid" do
    assert @group.valid?
  end

  test "name should be present" do
    @group.name = ""
    assert_not @group.valid?
  end

  test "description should be present" do
    @group.description = ""
    assert_not @group.valid?
  end
  
  test "name should not be too long" do
    @group.name = "a" * 101
    assert_not @group.valid?
  end

  test "description should not be too long" do
    @group.description = "a" * 10001
    assert_not @group.valid?
  end
  
  test "name should be unique" do
    @group.save
    duplicate_client = @group.dup
    assert_not duplicate_client.valid?
  end

  test "associated usergroups should be destroyed" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)
    @group.save
    @group.user_groups.create!( user_id: user0.id)
    assert_difference 'UserGroup.count', -1 do
      @group.destroy
    end
  end
  
  test "should connect and disconnect a user" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)  
    group0 = groups(:fGroup0)                                                   #group from groups.yml (persisted)
    assert_not user0.connectedToGroup?(group0)
    assert_not group0.connectedToUser?(user0)
    group0.connectToUser(user0)
    assert user0.connectedToGroup?(group0)
    assert group0.connectedToUser?(user0)
    group0.disconnectFromUser(user0)
    assert_not user0.connectedToGroup?(group0)
    assert_not group0.connectedToUser?(user0)
  end
end