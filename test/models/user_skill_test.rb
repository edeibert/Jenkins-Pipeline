require 'test_helper'

class UserSkillTest < ActiveSupport::TestCase

  def setup
    @userskill = UserSkill.new( user_id: users(:fUser0).id, 
                                skill_id: skills(:fSkill0).id)
  end
  
  test "should be valid" do
    assert @userskill.valid?
  end

  test "user_id should be present" do
    @userskill.user_id = nil
    assert_not @userskill.valid?
  end

  test "skill_id should be present" do
    @userskill.skill_id = nil
    assert_not @userskill.valid?
  end
end
