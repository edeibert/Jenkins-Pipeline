require 'test_helper'

class ClientTest < ActiveSupport::TestCase

  def setup
    @client = Client.new(name: "oClient0", description: "oClient0Description")
  end
  
  #Variables
  startdate = DateTime.new(2016,1,1)
  enddate = DateTime.new(2016,12,31)
  
  test "should be valid" do
    assert @client.valid?
  end

  test "name should be present" do
    @client.name = ""
    assert_not @client.valid?
  end

  test "description should be present" do
    @client.description = ""
    assert_not @client.valid?
  end
  
  test "name should not be too long" do
    @client.name = "a" * 101
    assert_not @client.valid?
  end

  test "description should not be too long" do
    @client.description = "a" * 10001
    assert_not @client.valid?
  end
  
  test "name should be unique" do
    @client.save
    duplicate_client = @client.dup
    assert_not duplicate_client.valid?
  end

  test "associated userclients should be destroyed" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)
    @client.save
    @client.user_clients.create!( user_id: user0.id, 
                                  from_date: startdate, 
                                  to_date: enddate)
    assert_difference 'UserClient.count', -1 do
      @client.destroy
    end
  end
  
  test "should connect and disconnect a user" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)  
    client0  = clients(:fClient0)                                               #client from clients.yml (persisted)
    assert_not user0.connectedToClient?(client0)
    assert_not client0.connectedToUser?(user0)
    client0.connectToUser(user0, startdate, enddate)
    assert user0.connectedToClient?(client0)
    assert client0.connectedToUser?(user0)
    client0.disconnectFromUser(user0, startdate, enddate)
    assert_not user0.connectedToClient?(client0)
    assert_not client0.connectedToUser?(user0)
  end
end