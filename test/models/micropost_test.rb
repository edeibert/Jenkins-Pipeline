require 'test_helper'

class MicropostTest < ActiveSupport::TestCase

  def setup
    @micropost = users(:fUser0).microposts.build(content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "content should be present" do
    @micropost.content = ""
    assert_not @micropost.valid?
  end

  test "content should be at most 250 characters" do
    @micropost.content = "a" * 251
    assert_not @micropost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal microposts(:fMicropost0_User1), Micropost.first
  end
end
