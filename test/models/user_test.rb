require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new( name: "oUser0", 
                      gender: "Man",
                      residence: "Utrecht",
                      dateofbirth: "2000-01-01",
                      email: "oUser0@test.tst", 
                      phonenumber: "0123456789",
                      function_level: "1",
                      startdate: "2015-01-01",
                      password: "wachtwoord", 
                      password_confirmation: "wachtwoord")
                      
  end
      
  #Variables
  startdate = DateTime.new(2016,1,1)
  enddate = DateTime.new(2016,12,31)
  
  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 101
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 89 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "password should have a maximum length" do
    @user.password = @user.password_confirmation = "a" * 101
    assert_not @user.valid?
  end
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "associated userclients should be destroyed" do
    client0  = clients(:fClient0)                                               #client from clients.yml (persisted)                                              
    @user.save
    @user.user_clients.create!( client_id: client0.id, 
                                from_date: startdate, 
                                to_date: enddate)
    assert_difference 'UserClient.count', -1 do
      @user.destroy
    end
  end
  
  test "associated usergroups should be destroyed" do
    group0  = groups(:fGroup0)                                                  #group from clients.yml (persisted)                                              
    @user.save
    @user.user_groups.create!(group_id: group0.id, 
                              from_date: startdate, 
                              to_date: enddate)
    assert_difference 'UserGroup.count', -1 do
      @user.destroy
    end
  end
  
  test "associated usercourses should be destroyed" do
    course0  = courses(:fCourse0)                                               #course from clients.yml (persisted)                                              
    @user.save
    @user.user_courses.create!(course_id: course0.id)
    assert_difference 'UserCourse.count', -1 do
      @user.destroy
    end
  end
  
  test "associated userskills should be destroyed" do
    skill0  = skills(:fSkill0)                                                  #skill from clients.yml (persisted)                                              
    @user.save
    @user.user_skills.create!(skill_id: skill0.id, level: "Beginner")
    assert_difference 'UserSkill.count', -1 do
      @user.destroy
    end
  end
  
  test "should connect and disconnect a client/group/course/skill" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)  
    client0  = clients(:fClient0)                                               #client from clients.yml (persisted)
    group0 = groups(:fGroup0)                                                   #group from groups.yml (persisted)
    course0  = courses(:fCourse0)                                               #course from courses.yml (persisted)
    skill0 = skills(:fSkill0)                                                   #skill from skills.yml (persisted)
    assert_not user0.connectedToClient?(client0)
    assert_not user0.connectedToGroup?(group0)
    assert_not user0.connectedToCourse?(course0)
    assert_not user0.connectedToSkill?(skill0)
    assert_not client0.connectedToUser?(user0)
    assert_not group0.connectedToUser?(user0)
    assert_not course0.connectedToUser?(user0)
    assert_not skill0.connectedToUser?(user0)
    user0.connectToClient(client0, startdate, enddate)
    user0.connectToGroup(group0, startdate, enddate)
    user0.connectToCourse(course0)
    user0.connectToSkill(skill0)
    assert user0.connectedToClient?(client0)
    assert user0.connectedToGroup?(group0)
    assert user0.connectedToCourse?(course0)
    assert user0.connectedToSkill?(skill0)
    assert client0.connectedToUser?(user0)
    assert group0.connectedToUser?(user0)
    assert course0.connectedToUser?(user0)
    assert skill0.connectedToUser?(user0)
    user0.disconnectFromClient(client0, startdate, enddate)
    user0.disconnectFromGroup(group0, startdate, enddate)
    user0.disconnectFromCourse(course0)
    user0.disconnectFromSkill(skill0)
    assert_not user0.connectedToClient?(client0)
    assert_not user0.connectedToGroup?(group0)
    assert_not user0.connectedToCourse?(course0)
    assert_not user0.connectedToSkill?(skill0)
    assert_not client0.connectedToUser?(user0)
    assert_not group0.connectedToUser?(user0)
    assert_not course0.connectedToUser?(user0)
    assert_not skill0.connectedToUser?(user0)
  end
end