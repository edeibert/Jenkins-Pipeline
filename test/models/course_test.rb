require 'test_helper'

class CourseTest < ActiveSupport::TestCase

  def setup
    @course = Course.new( name: "oCourse0", description: "oCourse0Description")
  end
  
  test "should be valid" do
    assert @course.valid?
  end

  test "name should be present" do
    @course.name = ""
    assert_not @course.valid?
  end

  test "description should be present" do
    @course.description = ""
    assert_not @course.valid?
  end
  
  test "attitude should be present" do
    @course.attitude = ""
    assert_not @course.valid?
  end
  
  test "name should not be too long" do
    @course.name = "a" * 101
    assert_not @course.valid?
  end

  test "description should not be too long" do
    @course.description = "a" * 10001
    assert_not @course.valid?
  end
  
  test "name should be unique" do
    @course.save
    duplicate_course = @course.dup
    assert_not duplicate_course.valid?
  end

  test "associated usercourses should be destroyed" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)
    @course.save
    @course.user_courses.create!( user_id: user0.id)
    assert_difference 'UserCourse.count', -1 do
      @course.destroy
    end
  end
  
  test "should connect and disconnect a user" do
    user0 = users(:fUser0)                                                      #user from users.yml (persisted)  
    course0 = courses(:fCourse0)                                                #course from courses.yml (persisted)
    assert_not user0.connectedToCourse?(course0)
    assert_not course0.connectedToUser?(user0)
    course0.connectToUser(user0)
    assert user0.connectedToCourse?(course0)
    assert course0.connectedToUser?(user0)
    course0.disconnectFromUser(user0)
    assert_not user0.connectedToCourse?(course0)
    assert_not course0.connectedToUser?(user0)
  end
end