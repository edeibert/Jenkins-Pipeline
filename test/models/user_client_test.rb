require 'test_helper'

class UserClientTest < ActiveSupport::TestCase
  
  def setup
    @userclient = UserClient.new( user_id: users(:fUser0).id, 
                                  client_id: clients(:fClient0).id, 
                                  from_date: DateTime.new(2016,1,1), 
                                  to_date: DateTime.new(2016,12,31))
  end
  
  test "should be valid" do
    assert @userclient.valid?
  end

  test "user_id should be present" do
    @userclient.user_id = nil
    assert_not @userclient.valid?
  end

  test "client_id should be present" do
    @userclient.client_id = nil
    assert_not @userclient.valid?
  end
  
  test "from_date should be present" do
    @userclient.from_date = nil
    assert_not @userclient.valid?
  end

  test "to_date should be present" do
    @userclient.to_date = nil
    assert_not @userclient.valid?
  end
  
  test "to_date should be after from_date" do
    @userclient.to_date = @userclient.from_date - 1
    assert_not @userclient.valid?
  end
end
