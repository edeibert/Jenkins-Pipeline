require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "KZA Connected"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | KZA Connected"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "Over ons | KZA Connected"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | KZA Connected"
  end
end