module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "KZA Connected"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # Returns amount of characters to be displayed in panels
  def description_length_index_pages()
    375
  end
  
  def description_rows_new_and_edit_pages()
    10
  end
  
  def default_select_value()
    "-Kies een waarde-"
  end
  
  def default_timezone()
    "Amsterdam"
  end
    
  
  # Returns the semantic class of the flash message.
  def flash_class(level)
    case level
      when "success" then "ui success message"
      when "error" then "ui error message"
      when "notice" then "ui info message"
    end
  end

  #Returns class name (and thereby color) for Semantic ui ribbon based upon attitude. 
  def attitude_class(attitude)
    case attitude
      when "Test"  then "ui white ribbon label button"
      when "Business" then "ui pink ribbon label button"
      when "Technical" then "ui blue ribbon label button"
    end
  end 
  
  #Returns profile picture link based upon user name. 
  def user_avatar_class(name)
     "users/" + name.downcase.gsub(/\s+/, "") + ".jpg"
  end
  
end