class CoursesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :show, :index, :edit, :update, :destroy]
  before_action :admin_user,     only: [:destroy]
  
  def new
    @course = Course.new
  end
  
  def create
    @course = Course.new(course_params)
    if @course.save
      flash[:success] = "Nieuwe cursus aangemaakt!"
      redirect_to @course
    else
      render 'new'
    end
  end
  
  def show
    @course = Course.find(params[:id])
  end
  
  def index
    @courses = Course.all
    if params[:search]
      @courses = Course.search(params[:search]).paginate(page: params[:page], per_page: 10).order("name ASC")
    else
      @courses = @courses.paginate(page: params[:page], per_page: 10).order("name ASC")
    end
  end

  def edit
    @course = Course.find(params[:id])
  end
  
  def update
    @course = Course.find(params[:id])
    if @course.update_attributes(course_params) 
      flash[:success] = "Cursus succesvol bijgewerkt."
      redirect_to @course
    else
      render 'edit'
    end
  end
  
  def destroy
    @course = Course.find(params[:id])
    @course.destroy
    flash[:error] = "Cursus succesvol verwijderd."
    redirect_to courses_url
  end
  
  private

    def course_params
      params.require(:course).permit(:attitude, :name, :teacher, :amountofdays, :function_level, :accomplishments, :status, :startdate, :max_participants, :description)
    end
end