class SessionsController < ApplicationController
  
  def new
    if logged_in?
      redirect_to home_path
    end
  end  
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_back_or home_path
    else
      flash.now[:error] = "Invalide Email adres/Wachtwoord combinatie"
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
