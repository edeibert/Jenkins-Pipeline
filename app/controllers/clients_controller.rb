class ClientsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :show, :index, :edit, :update, :destroy]
  before_action :admin_user,     only: [:destroy]
  
  def new
    @client = Client.new
  end
  
  def create
    @client = Client.new(client_params)
    if @client.save
      flash[:success] = "Nieuwe klant aangemaakt!"
      redirect_to @client
    else
      render 'new'
    end
  end
  
  def show
    @client = Client.find(params[:id])
  end
  
  def index
    @clients = Client.all
    if params[:search]
      @clients = Client.search(params[:search]).paginate(page: params[:page], per_page: 10).order("name ASC")
    else
      @clients = @clients.paginate(page: params[:page], per_page: 10).order("name ASC")
    end
  end
  
  def edit
    @client = Client.find(params[:id])
  end
  
  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(client_params) 
      flash[:success] = "Klant succesvol bijgewerkt."
      redirect_to @client
    else
      render 'edit'
    end
  end
  
  def destroy
    @client = Client.find(params[:id])
    @client.destroy
    flash[:error] = "Klant succesvol verwijderd."
    redirect_to clients_url
  end
  
  private
  
    def client_params
      params.require(:client).permit(:attitude, :name, :branch, :bu_manager, :logo_url, :description)
    end
end