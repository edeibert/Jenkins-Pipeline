class SkillsController < ApplicationController
  before_action :logged_in_user,  only: [:new, :create, :show, :index, :edit, :update, :destroy]
  before_action :admin_user,      only: [:destroy]
  
  def new
    @skill = Skill.new
  end
  
  def create
    @skill = Skill.new(skill_params)
    if @skill.save
      flash[:success] = "Nieuw kennisgebied aangemaakt!"
      redirect_to @skill
    else
      render 'new'
    end
  end
  
  def show
    @skill = Skill.find(params[:id])
  end
  
  def index
    @skills = Skill.all
    if params[:search]
      @skills = Skill.search(params[:search]).paginate(page: params[:page], per_page: 10).order("name ASC")
    else
      @skills = @skills.paginate(page: params[:page], per_page: 10).order("name ASC")
    end
  end
  
  def edit
    @skill = Skill.find(params[:id])
  end
  
  def update
    @skill = Skill.find(params[:id])
    if @skill.update_attributes(skill_params) 
      flash[:success] = "Kennisgebied succesvol bijgewerkt."
      redirect_to @skill
    else
      render 'edit'
    end
  end
  
  def destroy
    @skill = Skill.find(params[:id])
    @skill.destroy
    flash[:error] = "Kennisgebied succesvol verwijderd."
    redirect_to skills_url
  end
  
  private

    def skill_params
      params.require(:skill).permit(:attitude, :name, :category, :description)
    end
end
