class UserGroupsController < ApplicationController
  before_action :logged_in_user

  def create
    usergroup = UserGroup.new(user_group_params)
    usergroup.save
    redirect_to groups_url
  end

  def destroy
    usergroup = UserGroup.find(params[:id])
    current_user.user_groups.delete(usergroup)
    redirect_to groups_url
  end
  
  private

    def user_group_params
      params.require(:user_group).permit(:group_id, :user_id)
    end
end