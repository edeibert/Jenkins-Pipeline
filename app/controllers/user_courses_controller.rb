class UserCoursesController < ApplicationController
  before_action :logged_in_user

  def create
    usercourse = UserCourse.new(user_course_params)
    usercourse.save
    redirect_to courses_url
  end

  def destroy
    usercourse = UserCourse.find(params[:id])
    current_user.user_courses.delete(usercourse)
    redirect_to courses_url
  end
  
  private

    def user_course_params
      params.require(:user_course).permit(:course_id, :user_id, :status)
    end
end