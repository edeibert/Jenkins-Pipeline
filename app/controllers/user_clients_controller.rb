class UserClientsController < ApplicationController
  before_action :logged_in_user

  def create
    userclient = UserClient.new(user_client_params)
    userclient.save
    redirect_to clients_url
  end

  def destroy
    userclient = UserClient.find(params[:id])
    current_user.user_clients.delete(userclient)
    redirect_to clients_url
  end
  
  private

    def user_client_params
      params.require(:user_client).permit(:client_id, :user_id, :from_date, :to_date)
    end
end