class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include SessionsHelper

  private
    # Before filters:
    def logged_in_user
      unless logged_in?
        store_location
        flash[:error] = "Je dient ingelogd te zijn om deze pagina te bekijken."
        redirect_to login_url
      end
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    
    def set_select_collections
      @courses = Course.all
      @groups = Group.all
      @skills = Skill.all
      @clients = Client.all
    end
end
