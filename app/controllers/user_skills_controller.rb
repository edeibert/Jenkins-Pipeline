class UserSkillsController < ApplicationController
  before_action :logged_in_user
  
  def create
    userskill = UserSkill.new(user_skill_params)
    userskill.save
    redirect_to skills_url
  end

  def destroy
    userskill = UserSkill.find(params[:id])
    current_user.user_skills.delete(userskill)
    redirect_to skills_url
  end
  
  private

    def user_skill_params
      params.require(:user_skill).permit(:skill_id, :user_id, :level)
    end
end