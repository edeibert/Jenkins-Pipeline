class GroupsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :show, :index, :edit, :update, :destroy]
  before_action :admin_user,     only: [:destroy]
  
  def new
    @group = Group.new
  end
  
  def create
    @group = Group.new(group_params)
    if @group.save
      flash[:success] = "Nieuwe groep aangemaakt!"
      redirect_to @group
    else
      render 'new'
    end
  end
  
  def show
    @group = Group.find(params[:id])
  end
  
  def index
    @groups = Group.all
    if params[:search]
      @groups = Group.search(params[:search]).paginate(page: params[:page], per_page: 10).order("name ASC")
    else
      @groups = @groups.paginate(page: params[:page], per_page: 10).order("name ASC")
    end
  end
  
  def edit
    @group = Group.find(params[:id])
  end
  
  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(group_params) 
      flash[:success] = "Groep succesvol bijgewerkt."
      redirect_to @group
    else
      render 'edit'
    end
  end
  
  def destroy
    @group = Group.find(params[:id])
    @group.destroy
    flash[:error] = "Groep succesvol verwijderd."
    redirect_to groups_url
  end
  
  private

    def group_params
      params.require(:group).permit(:attitude, :name, :email, :contact, :description)
    end
end