class UsersController < ApplicationController
  before_action :logged_in_user,          only: [:show, :index, :edit, :update, :destroy]
  before_action :correct_user,            only: [:edit, :update]
  before_action :set_select_collections,  only: [:update]
  before_action :admin_user,              only: [:new, :create, :destroy]

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] =  "Gebruiker '" + @user.name + "' aangemaakt!"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page], per_page: 10)
    @userage = age(@user.dateofbirth).to_s
  end
  
  def index
    @users = User.where.not(name: ['KZA Connected', current_user.name]) 
    if params[:search].present?
      @users = User.search(params[:search]).paginate(page: params[:page], per_page: 10).order("name ASC")
    else
      @users = @users.paginate(page: params[:page], per_page: 10).order("name ASC")
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def edit_connections
    @user = User.find(params[:id])
  end
  
  def update
    if @user.update_attributes(user_params) 
      flash[:success] = "Profiel succesvol bijgewerkt."
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def edit_connections
    @user = User.find(params[:id])
  end
  
  def update_connections
    if @user.update_attributes(user_params) 
      flash[:success] = "Connecties succesvol bijgewerkt."
      redirect_to @user
    else
      render 'edit_connections'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:error] = "Account succesvol verwijderd."
    redirect_to users_url
  end
  
  private
    def age(dob)
      now = Time.now.utc.to_date
      now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
    end
  
    def user_params
      params.require(:user).permit(:attitude, :name, :gender, :residence, :dateofbirth, :email, :phonenumber, :function_level, :startdate, :password, :password_confirmation, :bu_manager, :is_bu_manager, :description,
                                    user_courses_attributes: [:id, :user_id, :course_id, :status, :_destroy],
                                    user_groups_attributes: [:id, :user_id, :group_id, :_destroy],
                                    user_skills_attributes: [:id, :user_id, :skill_id, :level, :_destroy],
                                    user_clients_attributes: [:id, :user_id, :from_date, :to_date, :client_id, :_destroy])
    end
end