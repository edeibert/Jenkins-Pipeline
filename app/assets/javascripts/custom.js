function replaceClassName(oldClassName, newClassName)
{
    var elements = document.getElementsByClassName(oldClassName);

    for(var i = (elements.length - 1); i >= 0; i--)
    {
        elements[i].className = newClassName;
    }
}

function setClassNameOfAllChilds(parentClassName, childsClassName)
{
    var elements = document.getElementsByClassName(parentClassName);
    
    for(var i = (elements.length - 1); i >= 0; i--)
    {
        for(var j = (elements[i].children.length - 1); j >= 0; j--)
        {
            if (j == 0) {
                elements[i].children[j].innerHTML = "";
                var leftIcon = document.createElement("i");
                leftIcon.className = "angle left icon";
                elements[i].children[j].appendChild(leftIcon);
            }
            if (j == (elements[i].children.length - 1)){
                elements[i].children[j].innerHTML = "";
                var rightIcon = document.createElement("i");
                rightIcon.className = "angle right icon";
                elements[i].children[j].appendChild(rightIcon);
            }

            elements[i].children[j].className += " " + childsClassName;
        }
    }
}