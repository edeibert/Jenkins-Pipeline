class Course < ActiveRecord::Base
  has_many :user_courses, foreign_key: "course_id", dependent: :destroy
  has_many :users, through: :user_courses
  
  validates :attitude, presence: true, length: { maximum: 10 }
  validates :name, presence: true, length: { maximum: 100 }, uniqueness: { case_sensitive: false }
  validates :teacher, presence: true, length: { maximum: 100 }
  validates :amountofdays, presence: true
  validates :function_level, presence: true, length: { maximum: 100 }
  validates :accomplishments, presence: true, length: { maximum: 100 }
  validates :status, presence: true, length: { maximum: 100 }
  validates :startdate, presence: true
  validates :max_participants, presence: true
  validates :description, presence: true, length: { maximum: 1000 } 
  
  # Searches in name and description
  def self.search(search)
    search = search.downcase
    wildcard_search = "%#{search}%"
    where("lower(attitude) LIKE ? OR " +
          "lower(name) LIKE ? OR " +
          "lower(teacher) LIKE ? OR " +
          "lower(accomplishments) LIKE ? OR " +
          "lower(status) LIKE ? OR " +
          "lower(description) LIKE ?", 
          wildcard_search, wildcard_search, 
          wildcard_search, wildcard_search,
          wildcard_search, wildcard_search)
  end
end
