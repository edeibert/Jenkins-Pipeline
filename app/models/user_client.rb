class UserClient < ActiveRecord::Base
  belongs_to :user
  belongs_to :client
  
  default_scope -> { order(to_date: :desc, from_date: :desc, user_id: :asc, client_id: :asc) }

  validates :client_id, presence: true
  validates :from_date, presence: true
  validates :to_date, presence: true
  validate :to_date_after_from_date

  
  private

    def to_date_after_from_date
      return if to_date.blank? || from_date.blank?
     
      if to_date < from_date
            errors.add(:to_date, "De startdatum moet vóór de einddatum liggen") 
      end 
    end
end