class UserCourse < ActiveRecord::Base
  belongs_to :user
  belongs_to :course
  
  default_scope -> { order(status: :desc, created_at: :asc, user_id: :asc, course_id: :asc) }

  validates :course_id, presence: true
  validates :status, presence: true
end
