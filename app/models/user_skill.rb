class UserSkill < ActiveRecord::Base
  belongs_to :user
  belongs_to :skill
  
  default_scope -> { order(level: :asc, user_id: :asc, skill_id: :asc) }

  validates :skill_id, presence: true
  validates :level, presence: true
end
