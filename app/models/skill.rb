class Skill < ActiveRecord::Base
  has_many :user_skills, foreign_key: "skill_id", dependent: :destroy
  has_many :users, through: :user_skills
  
  validates :attitude, presence: true, length: { maximum: 10 }
  validates :name, presence: true, length: { maximum: 100 }, uniqueness: { case_sensitive: false }
  validates :category, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 1000 }

  # Searches in name and description
  def self.search(search)
    search = search.downcase
    wildcard_search = "%#{search}%"
    where("lower(attitude) LIKE ? OR " +
          "lower(name) LIKE ? OR " +
          "lower(category) LIKE ? OR " +
          "lower(description) LIKE ?", 
          wildcard_search, wildcard_search,
          wildcard_search, wildcard_search)
  end
end
