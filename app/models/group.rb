class Group < ActiveRecord::Base
  has_many :user_groups, foreign_key: "group_id", dependent: :destroy
  has_many :users, through: :user_groups
  
  before_save { email.downcase! }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  
  validates :attitude, presence: true, length: { maximum: 10 }
  validates :name, presence: true, length: { maximum: 100 }, uniqueness: { case_sensitive: false }
  validates :email, presence: true, length: { maximum: 100 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :contact, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 1000 }
  
  # Searches in name and description
  def self.search(search)
    search = search.downcase
    wildcard_search = "%#{search}%"
    where("lower(attitude) LIKE ? OR " +
          "lower(name) LIKE ? OR " +
          "lower(email) LIKE ? OR " +
          "lower(contact) LIKE ? OR " +
          "lower(description) LIKE ?", 
          wildcard_search, wildcard_search, 
          wildcard_search, wildcard_search,
          wildcard_search)
  end
  
  #Users
    # Connects Group to User.
    def connectToUser(connecting_user)
      user_groups.create(user_id: connecting_user.id)
    end
  
    # Disconnects Group to User.
    def disconnectFromUser(connected_user)
      user_groups.find_by(user_id: connected_user.id).destroy
    end
    
    # Returns true if the Group is connected to the User.
    def connectedToUser?(random_user)
      users.include?(random_user)
    end
end
