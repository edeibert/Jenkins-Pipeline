class Micropost < ActiveRecord::Base
  belongs_to :user
  
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader 
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 250 }
  validate  :picture_size
  
  private

    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "De maximaal toegestane up te loaden bestandsgrootte is 5MB.")
      end
    end
end
