class UserGroup < ActiveRecord::Base
  belongs_to :user
  belongs_to :group  

  default_scope -> { order(user_id: :asc, group_id: :asc) }

  validates :group_id, presence: true
end