class User < ActiveRecord::Base
  has_many :microposts, dependent: :destroy
  has_many :user_clients, foreign_key: "user_id", dependent: :destroy, inverse_of: :user
  has_many :user_groups, foreign_key: "user_id", dependent: :destroy, inverse_of: :user
  has_many :user_skills, foreign_key: "user_id", dependent: :destroy, inverse_of: :user
  has_many :user_courses, foreign_key: "user_id", dependent: :destroy, inverse_of: :user
  has_many :clients, through: :user_clients
  has_many :groups, through: :user_groups
  has_many :skills, through: :user_skills
  has_many :courses, through: :user_courses
  accepts_nested_attributes_for :user_courses, allow_destroy: true
  accepts_nested_attributes_for :user_groups, allow_destroy: true
  accepts_nested_attributes_for :user_skills, allow_destroy: true
  accepts_nested_attributes_for :user_clients, allow_destroy: true
  attr_accessor :remember_token
  
  before_save { email.downcase! }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  has_secure_password
    
  validates :attitude, presence: true, length: { maximum: 10 }
  validates :name, presence: true, length: { maximum: 100 }
  validates :gender, presence: true
  validates :residence, presence: true, length: { maximum: 100 }
  validates :dateofbirth, presence: true
  validates :email, presence: true, length: { maximum: 100 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :phonenumber, presence: true, length: { minimum: 10, maximum: 10 }
  validates :function_level, presence: true, length: { minimum: 1, maximum: 1 }
  validates :description, presence: true, length: { maximum: 1000 }
  validates :startdate, presence: true
  validates :password, presence: true, length: { minimum: 6, maximum: 100 }, allow_nil: true 


    
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    Micropost.limit(10)
  end
  
  # Searches in name and email
  def self.search(search)
    search = search.downcase
    wildcard_search = "%#{search}%"
    where(  "lower(attitude) LIKE ? OR " +
            "lower(name) LIKE ? OR " +
            "lower(gender) LIKE ? OR " +
            "lower(residence) LIKE ? OR " +
            "lower(phonenumber) LIKE ? OR " +
            "lower(email) LIKE ? OR " +
            "lower(description) LIKE ?", 
            wildcard_search, wildcard_search, 
            wildcard_search, wildcard_search,
            wildcard_search, wildcard_search,
            wildcard_search)
  end
end