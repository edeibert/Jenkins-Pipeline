# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171002192350) do

  create_table "clients", force: :cascade do |t|
    t.string   "attitude"
    t.string   "name"
    t.string   "branch"
    t.string   "bu_manager"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "logo_url"
  end

  create_table "courses", force: :cascade do |t|
    t.string   "attitude"
    t.string   "name"
    t.string   "teacher"
    t.string   "amountofdays"
    t.string   "function_level"
    t.string   "accomplishments"
    t.string   "status"
    t.date     "startdate"
    t.string   "max_participants"
    t.text     "description"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "attitude"
    t.string   "name"
    t.string   "email"
    t.string   "contact"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "microposts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.string   "picture"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "microposts", ["user_id", "created_at"], name: "index_microposts_on_user_id_and_created_at"
  add_index "microposts", ["user_id"], name: "index_microposts_on_user_id"

  create_table "skills", force: :cascade do |t|
    t.string   "attitude"
    t.string   "name"
    t.string   "category"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "user_clients", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "client_id"
    t.datetime "from_date"
    t.datetime "to_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_clients", ["client_id"], name: "index_user_clients_on_client_id"
  add_index "user_clients", ["user_id", "client_id", "from_date", "to_date"], name: "index_user_clients_on_all_properties", unique: true
  add_index "user_clients", ["user_id"], name: "index_user_clients_on_user_id"

  create_table "user_courses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_courses", ["course_id"], name: "index_user_courses_on_course_id"
  add_index "user_courses", ["user_id", "course_id"], name: "index_user_courses", unique: true
  add_index "user_courses", ["user_id"], name: "index_user_courses_on_user_id"

  create_table "user_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_groups", ["group_id"], name: "index_user_groups_on_group_id"
  add_index "user_groups", ["user_id", "group_id"], name: "index_user_groups", unique: true
  add_index "user_groups", ["user_id"], name: "index_user_groups_on_user_id"

  create_table "user_skills", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "skill_id"
    t.string   "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_skills", ["skill_id"], name: "index_user_skills_on_skill_id"
  add_index "user_skills", ["user_id", "skill_id"], name: "index_user_skills", unique: true
  add_index "user_skills", ["user_id"], name: "index_user_skills_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "attitude"
    t.string   "name"
    t.string   "gender"
    t.datetime "dateofbirth"
    t.string   "residence"
    t.string   "email"
    t.string   "phonenumber"
    t.datetime "startdate"
    t.datetime "enddate"
    t.string   "function_level"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_bu_manager",   default: false
    t.string   "bu_manager"
    t.text     "description"
  end

end
