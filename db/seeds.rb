#Sample Admin User (1)
User.create!(
             attitude: "Technical",
             name:  "Wouter Neve",
             gender: "Man",
             residence: "Utrecht",
             dateofbirth: DateTime.new(1987,6,26),
             email: "wneve@kza.nl",
             phonenumber: "0627129536",
             function_level: "1",
             startdate: DateTime.new(2014,10,1),
             password: "Welkom01",
             password_confirmation: "Welkom01",
             is_bu_manager: false,
             bu_manager: "Frank Lansink",
             description: "bla",
             admin: true)