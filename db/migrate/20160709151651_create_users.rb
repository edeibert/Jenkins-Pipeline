class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string    :attitude
      t.string    :name
      t.string    :gender
      t.datetime  :dateofbirth
      t.string    :residence
      t.string    :email, unique: true
      t.string    :phonenumber
      t.datetime  :startdate
      t.datetime  :enddate, default: nil
      t.string    :function_level
      t.string    :password_digest
      t.string    :remember_digest
      t.boolean   :admin, default: false
      
      t.timestamps null: false
    end  
  end
end
