class AddBuManagerToUser < ActiveRecord::Migration
  def change
    add_column :users, :is_bu_manager, :boolean, default: false
    add_column :users, :bu_manager, :string
  end
end
