class CreateUserClients < ActiveRecord::Migration
  def change
    create_table :user_clients do |t|
      t.integer :user_id
      t.integer :client_id
      t.datetime :from_date
      t.datetime :to_date

      t.timestamps null: false
    end
    add_index :user_clients, :user_id
    add_index :user_clients, :client_id
    add_index :user_clients, [:user_id, :client_id, :from_date, :to_date], unique: true, name: 'index_user_clients_on_all_properties'
  end
end
