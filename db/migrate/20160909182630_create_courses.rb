class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string    :attitude
      t.string    :name
      t.string    :teacher
      t.string    :amountofdays
      t.string    :function_level
      t.string    :accomplishments
      t.string    :status      
      t.date      :startdate
      t.string    :max_participants      
      t.text      :description

      t.timestamps null: false
    end
  end
end
