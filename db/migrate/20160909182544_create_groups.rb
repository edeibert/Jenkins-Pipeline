class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string  :attitude
      t.string  :name
      t.string  :email
      t.string  :contact
      t.text    :description

      t.timestamps null: false
    end
  end
end
